# hexagonal-demo

## Repository description

This is a demo REST API made with hexagonal architecture. The project contains the following modules:

- `infrastructure`: simple framework wrapper, Spring in this case, to avoid coupling.  
- `core`: application's business logic. Includes unit tests.
- `adapter-data-access`: implementation of the data access secondary adapter.
- `adapter-rest`: implementation of the REST primary adapter.
- `application`: module that builds the deployable application artifact. Includes integration tests.

## Running the API

Please, follow these steps:

1. Open the console in this directory.
2. Run `mvn clean install`.
3. Run `mvn spring-boot:run -pl hexagonal-demo-application`.
4. [Try the API with Swagger](http://localhost:8080/swagger-ui/index.html) or [make an HTTP call directly](http://localhost:8080/api/prices?brandId=1&productId=35455&date=2020-06-15T21:00:00Z).
