package com.hexagonal.demo;

import com.hexagonal.demo.api.rest.constant.ApiParams;
import com.hexagonal.demo.api.rest.constant.ApiPaths;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.StandardCharsets;
import java.util.stream.Stream;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class PriceIntegrationTest {

    private static final String BRAND_ID = "1";

    private static final String UNKNOWN_BRAND_ID = "7";

    private static final String PRODUCT_ID = "35455";

    private static final String UNKNOWN_PRODUCT_ID = "24";

    private static final String DATE_1 = "2020-06-14T10:00:00Z";

    private static final String DATE_2 = "2020-06-14T16:00:00Z";

    private static final String DATE_3 = "2020-06-14T21:00:00Z";

    private static final String DATE_4 = "2020-06-15T10:00:00Z";

    private static final String DATE_5 = "2020-06-16T21:00:00Z";

    private static final String TOO_EARLY_DATE = "2019-01-01T17:00:00Z";

    private static final String EMPTY_PARAM = "";

    @Autowired
    private MockMvc mockMvc;

    @ParameterizedTest
    @MethodSource("findByBrandIdAndProductIdAndDateSuccessCases")
    public void findByBrandIdAndProductIdAndDateSuccessTest(String date, String expectedJsonName) throws Exception {
            String expectedJson = IOUtils.resourceToString(expectedJsonName, StandardCharsets.UTF_8);
            this.mockMvc.perform(get(ApiPaths.Price.BASE)
                            .param(ApiParams.Price.BRAND_ID, BRAND_ID)
                            .param(ApiParams.Price.PRODUCT_ID, PRODUCT_ID)
                            .param(ApiParams.Price.DATE, date))
                    .andDo(print())
                    .andExpect(status().isOk())
                    .andExpect(content().json(expectedJson));
    }

    private static Stream<Arguments> findByBrandIdAndProductIdAndDateSuccessCases() {
        return Stream.of(
                Arguments.of(DATE_1, "/responseToTest1.json"),
                Arguments.of(DATE_2, "/responseToTest2.json"),
                Arguments.of(DATE_3, "/responseToTest3.json"),
                Arguments.of(DATE_4, "/responseToTest4.json"),
                Arguments.of(DATE_5, "/responseToTest5.json")
        );
    }

    @ParameterizedTest
    @MethodSource("findByBrandIdAndProductIdAndDateFailCases")
    public void findByBrandIdAndProductIdAndDateFailTest(String brandId, String productId, String date) throws Exception {
        this.mockMvc.perform(get(ApiPaths.Price.BASE)
                        .param(ApiParams.Price.BRAND_ID, brandId)
                        .param(ApiParams.Price.PRODUCT_ID, productId)
                        .param(ApiParams.Price.DATE, date))
                .andDo(print())
                .andExpect(status().is4xxClientError());
    }

    private static Stream<Arguments> findByBrandIdAndProductIdAndDateFailCases() {
        return Stream.of(
                Arguments.of(UNKNOWN_BRAND_ID, PRODUCT_ID, DATE_1),
                Arguments.of(BRAND_ID, UNKNOWN_PRODUCT_ID, DATE_1),
                Arguments.of(BRAND_ID, PRODUCT_ID, TOO_EARLY_DATE),
                Arguments.of(EMPTY_PARAM, PRODUCT_ID, DATE_1),
                Arguments.of(BRAND_ID, EMPTY_PARAM, DATE_1),
                Arguments.of(BRAND_ID, PRODUCT_ID, EMPTY_PARAM)
        );
    }
}
