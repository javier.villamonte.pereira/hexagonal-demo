package com.hexagonal.demo.persistence.price;

import com.hexagonal.demo.annotations.Inject;
import com.hexagonal.demo.component.price.domain.entity.Price;
import com.hexagonal.demo.component.price.port.PriceRepository;
import com.hexagonal.demo.sharedkernel.exception.AppError;
import com.hexagonal.demo.sharedkernel.exception.AppErrorType;
import com.hexagonal.demo.sharedkernel.exception.ResourceNotFoundRuntimeException;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.lang.invoke.MethodHandles;
import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class PriceRepositoryImpl implements PriceRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Setter(onMethod_ = @Inject)
    private PriceDao priceDao;

    @Setter(onMethod_ = @Inject)
    private PriceEntityMapper priceEntityMapper;

    @Override
    public List<Price> findByBrandIdAndProductIdAndDate(Integer brandId, Integer productId, Instant date) {
        LOGGER.debug("Querying for price by brand, product and date. BrandId: {}, ProductId: {}, Date: {}", brandId, productId, date);
        return this.priceDao.findByBrandIdAndProductIdAndStartDateLessThanEqualAndEndDateGreaterThanEqual(brandId, productId, date, date)
                .stream()
                .map(this.priceEntityMapper::toDomain)
                .collect(Collectors.toList());
    }
}
