package com.hexagonal.demo.persistence.price;

import com.hexagonal.demo.component.price.domain.entity.Price;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PriceEntityMapper {

    Price toDomain(PriceEntity entity);
}
