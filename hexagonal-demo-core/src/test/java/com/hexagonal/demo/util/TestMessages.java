package com.hexagonal.demo.util;

public final class TestMessages {

    public static final String TEST_NAME = "{0}";

    public static final String INVALID_PARAMS_EXPECTED = "An exception should be thrown when a param is not valid";

    public static final String VALID_PARAMS_EXPECTED = "An exception should not be thrown when all params are valid";

    private TestMessages() {
        super();
    }
}
