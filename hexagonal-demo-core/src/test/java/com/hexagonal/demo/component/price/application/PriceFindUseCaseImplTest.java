package com.hexagonal.demo.component.price.application;

import com.hexagonal.demo.component.price.domain.entity.Price;
import com.hexagonal.demo.component.price.domain.entity.PriceBuilder;
import com.hexagonal.demo.component.price.port.PriceFindUseCase;
import com.hexagonal.demo.component.price.port.PriceRepository;
import com.hexagonal.demo.sharedkernel.exception.ClientCausedRuntimeException;
import com.hexagonal.demo.util.TestConstants;
import com.hexagonal.demo.util.TestMessages;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Instant;
import java.util.List;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class PriceFindUseCaseImplTest {

    @Mock
    private PriceRepository priceRepository;

    private PriceFindUseCase priceFindUseCase;

    @BeforeEach
    public void setup() {
        PriceFindUseCaseImpl priceFindUseCaseImpl = new PriceFindUseCaseImpl();
        priceFindUseCaseImpl.setPriceRepository(this.priceRepository);

        this.priceFindUseCase = priceFindUseCaseImpl;
    }

    @ParameterizedTest(name = TestMessages.TEST_NAME)
    @MethodSource("findByBrandIdAndProductIdAndDateParameterValidationFailCases")
    public void findByBrandIdAndProductIdAndDateParameterValidationFailTest(String testName, Integer brandId, Integer productId, Instant date) {
        assertThatExceptionOfType(ClientCausedRuntimeException.class)
                .as(TestMessages.INVALID_PARAMS_EXPECTED)
                .isThrownBy(() -> this.priceFindUseCase.findByBrandIdAndProductIdAndDate(brandId, productId, date));
    }

    private static Stream<Arguments> findByBrandIdAndProductIdAndDateParameterValidationFailCases() {
        return Stream.of(
                Arguments.of("Param 'brandId' is null", null, TestConstants.INTEGER_ID, TestConstants.INSTANT_VALUE),
                Arguments.of("Param 'productId' is null", TestConstants.INTEGER_ID, null, TestConstants.INSTANT_VALUE),
                Arguments.of("Param 'date' is null", TestConstants.INTEGER_ID, TestConstants.INTEGER_ID, null)
        );
    }

    @Test
    public void findByBrandIdAndProductIdAndDateParameterValidationSuccessTest() {
        Price expectedResult = PriceBuilder.build3();
        Mockito.when(this.priceRepository.findByBrandIdAndProductIdAndDate(Mockito.anyInt(), Mockito.anyInt(), Mockito.any(Instant.class)))
                .thenReturn(List.of(PriceBuilder.build1(), PriceBuilder.build2(), PriceBuilder.build3()));
        
        Price actualResult = this.priceFindUseCase.findByBrandIdAndProductIdAndDate(TestConstants.INTEGER_ID, TestConstants.INTEGER_ID, TestConstants.INSTANT_VALUE);
        
        assertThat(actualResult.getId()).isEqualTo(expectedResult.getId());
    }
}
