package com.hexagonal.demo.component.price.domain.entity;

import java.time.Instant;

public class PriceBuilder {

    public static final int ID_1 = 1;

    public static final int ID_2 = 2;

    public static final int ID_3 = 2;

    public static final int PRIORITY_1 = 1;

    public static final int PRIORITY_2 = 2;

    public static final int PRIORITY_3 = 3;

    public static final int BRAND_ID = 1;

    public static final int PRODUCT_ID = 1;
    
    public static final Instant START_DATE = Instant.parse("2007-12-03T10:15:30.00Z");

    public static final Instant END_DATE = Instant.parse("2008-12-03T10:15:30.00Z");

    public static final double PRICE = 100;

    public static final String CURRENCY_ISO_CODE = "EUR";
    
    public static Price build1() {
        Price price = new Price();
        price.setId(ID_1);
        price.setBrandId(BRAND_ID);
        price.setProductId(PRODUCT_ID);
        price.setStartDate(START_DATE);
        price.setEndDate(END_DATE);
        price.setPriority(PRIORITY_1);
        price.setPrice(PRICE);
        price.setCurrencyIsoCode(CURRENCY_ISO_CODE);
        return price;
    }


    public static Price build2() {
        Price price = new Price();
        price.setId(ID_2);
        price.setBrandId(BRAND_ID);
        price.setProductId(PRODUCT_ID);
        price.setStartDate(START_DATE);
        price.setEndDate(END_DATE);
        price.setPriority(PRIORITY_2);
        price.setPrice(PRICE);
        price.setCurrencyIsoCode(CURRENCY_ISO_CODE);
        return price;
    }


    public static Price build3() {
        Price price = new Price();
        price.setId(ID_3);
        price.setBrandId(BRAND_ID);
        price.setProductId(PRODUCT_ID);
        price.setStartDate(START_DATE);
        price.setEndDate(END_DATE);
        price.setPriority(PRIORITY_3);
        price.setPrice(PRICE);
        price.setCurrencyIsoCode(CURRENCY_ISO_CODE);
        return price;
    }
}
