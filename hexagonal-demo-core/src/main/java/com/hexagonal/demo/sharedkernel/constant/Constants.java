package com.hexagonal.demo.sharedkernel.constant;

public final class Constants {

    public static final int FIRST_ELEMENT = 0;

    private Constants() {
        super();
    }
}
