package com.hexagonal.demo.sharedkernel.validation;

import com.hexagonal.demo.sharedkernel.exception.AppError;
import com.hexagonal.demo.sharedkernel.exception.AppErrorType;
import com.hexagonal.demo.sharedkernel.exception.ClientCausedRuntimeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;

public final class Assert {

    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    private Assert() {
        super();
    }

    public static void notNullParam(String paramName, Object paramValue) {
        if (paramValue == null) {
            LOGGER.debug("Mandatory param '{}' is null.", paramName);
            throw new ClientCausedRuntimeException(AppErrorType.NULL_PARAMETER.createInstance()
                    .addMeta(AppError.MetaKeys.PARAM_NAME, paramName));
        }
    }
}
