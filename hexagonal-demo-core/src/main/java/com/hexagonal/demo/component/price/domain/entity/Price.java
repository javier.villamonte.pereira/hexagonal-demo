package com.hexagonal.demo.component.price.domain.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.Instant;

@Getter
@Setter
@ToString
public class Price {

    public static final String ENTITY_NAME = "Price";

    public static final String BRAND_ID_FIELD = "brandId";

    public static final String PRODUCT_ID_FIELD = "productId";

    public static final String DATE_SEARCH_PARAM = "date";

    private Integer id;

    private Integer brandId;

    private Integer productId;

    private Instant startDate;

    private Instant endDate;

    private Integer priority;

    private Double price;

    private String currencyIsoCode;
}
