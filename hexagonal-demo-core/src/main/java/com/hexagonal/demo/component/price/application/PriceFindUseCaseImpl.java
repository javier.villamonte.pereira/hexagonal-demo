package com.hexagonal.demo.component.price.application;

import com.hexagonal.demo.annotations.ApplicationService;
import com.hexagonal.demo.annotations.Inject;
import com.hexagonal.demo.component.price.domain.entity.Price;
import com.hexagonal.demo.component.price.port.PriceFindUseCase;
import com.hexagonal.demo.component.price.port.PriceRepository;
import com.hexagonal.demo.sharedkernel.exception.AppError;
import com.hexagonal.demo.sharedkernel.exception.AppErrorType;
import com.hexagonal.demo.sharedkernel.exception.ResourceNotFoundRuntimeException;
import com.hexagonal.demo.sharedkernel.validation.Assert;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.time.Instant;
import java.util.Comparator;

@ApplicationService(readOnly = true)
public class PriceFindUseCaseImpl implements PriceFindUseCase {

    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Setter(onMethod_ = @Inject)
    private PriceRepository priceRepository;

    @Override
    public Price findByBrandIdAndProductIdAndDate(Integer brandId, Integer productId, Instant date) {
        LOGGER.debug("Use case for finding price by brand, product and date. BrandId: {}, ProductId: {}, Date: {}", brandId, productId, date);
        this.checkParams(brandId, productId, date);
        return this.priceRepository.findByBrandIdAndProductIdAndDate(brandId, productId, date)
                .stream()
                .max(Comparator.comparingInt(Price::getPriority))
                .orElseThrow(() -> new ResourceNotFoundRuntimeException(AppErrorType.ENTITY_NOT_FOUND.createInstance()
                        .addMeta(AppError.MetaKeys.ENTITY_NAME, Price.ENTITY_NAME)
                        .addMultipleMeta(AppError.MetaKeys.FIELD_NAMES,
                                Price.BRAND_ID_FIELD, Price.PRODUCT_ID_FIELD, Price.DATE_SEARCH_PARAM)
                        .addMultipleMeta(AppError.MetaKeys.FIELD_VALUES,
                                brandId, productId, date)));
    }

    private void checkParams(Integer brandId, Integer productId, Instant date) {
        Assert.notNullParam(Price.BRAND_ID_FIELD, brandId);
        Assert.notNullParam(Price.PRODUCT_ID_FIELD, productId);
        Assert.notNullParam(Price.DATE_SEARCH_PARAM, date);
    }
}
