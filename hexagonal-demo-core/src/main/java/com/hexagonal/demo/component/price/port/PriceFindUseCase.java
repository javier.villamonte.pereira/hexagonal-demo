package com.hexagonal.demo.component.price.port;

import com.hexagonal.demo.component.price.domain.entity.Price;

import java.time.Instant;

public interface PriceFindUseCase {

    Price findByBrandIdAndProductIdAndDate(Integer brandId, Integer productId, Instant date);
}
