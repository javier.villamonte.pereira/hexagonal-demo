package com.hexagonal.demo.component.price.port;

import com.hexagonal.demo.component.price.domain.entity.Price;

import java.time.Instant;
import java.util.List;

public interface PriceRepository {

    List<Price> findByBrandIdAndProductIdAndDate(Integer brandId, Integer productId, Instant date);
}
