package com.hexagonal.demo.api.rest.price;

import com.hexagonal.demo.component.price.domain.entity.Price;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PriceDtoMapper {

    PriceDto toDto(Price price);
}
