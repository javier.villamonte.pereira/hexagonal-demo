package com.hexagonal.demo.api.rest.price;

import com.hexagonal.demo.annotations.Inject;
import com.hexagonal.demo.api.rest.constant.ApiParams;
import com.hexagonal.demo.api.rest.constant.ApiPaths;
import com.hexagonal.demo.component.price.domain.entity.Price;
import com.hexagonal.demo.component.price.port.PriceFindUseCase;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.info.Info;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.lang.invoke.MethodHandles;
import java.time.Instant;

@OpenAPIDefinition(
        info = @Info(
                title = "Price API",
                version = "0.1.0",
                description = "API that allows finding product prices"
        )
)
@RestController
@RequestMapping(ApiPaths.Price.BASE)
public class PriceController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Setter(onMethod_ = @Inject)
    private PriceFindUseCase priceFindUseCase;

    @Setter(onMethod_ = @Inject)
    private PriceDtoMapper priceDtoMapper;

    @Operation(summary = "Find the price of a product on a specific date")
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PriceDto> findByBrandIdAndProductIdAndDate(
            @Parameter(description = "Numeric brand identifier", example = "1")
            @RequestParam(name = ApiParams.Price.BRAND_ID) Integer brandId,
            @Parameter(description = "Numeric product identifier", example = "35455")
            @RequestParam(name = ApiParams.Price.PRODUCT_ID) Integer productId,
            @Parameter(description = "Date and time in UTC (ISO 8601) on which you want to find the product price", example = "2020-06-15T21:00:00Z")
            @RequestParam(name = ApiParams.Price.DATE) Instant date) {
        LOGGER.debug("Request for finding price by brand, product and date. BrandId: {}, ProductId: {}, Date: {}",
                brandId, productId, date);
        Price price = this.priceFindUseCase.findByBrandIdAndProductIdAndDate(brandId, productId, date);
        return ResponseEntity.ok(this.priceDtoMapper.toDto(price));
    }
}
